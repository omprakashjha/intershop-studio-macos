#!/bin/bash

CURL="/usr/bin/curl"
TAR="/usr/bin/tar"
SED="/usr/bin/sed"

MAC_ECLIPSE_URL="http://ftp.snt.utwente.nl/pub/software/eclipse/eclipse/downloads/drops4/R-4.6.1-201609071200/eclipse-SDK-4.6.1-macosx-cocoa-x86_64.tar.gz"
IS_ARCHIVE="/Research/ish/IntershopStudio_4.6.1.66-linux.gtk.x86_64.zip"

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DOWNLOAD_DIR="$DIR/download"
TMP="$DIR/tmp-$(date +%s)"

# These are very specific to IntershopStudio / Eclipse versions (obviously)
LIBRARIES_TO_REMOVE=(
  org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.400.v20160518-1444
  org.eclipse.core.filesystem.linux.x86_64_1.2.200.v20140124-1940.jar
  org.eclipse.core.net.linux.x86_64_1.2.0.v20160323-1650.jar
  org.eclipse.e4.ui.swt.gtk_1.0.100.v20160301-1001.jar
  org.eclipse.swt.gtk.linux.x86_64_3.105.1.v20160907-0248.jar
)

LIBRARIES_TO_ADD=(
  org.eclipse.equinox.launcher.cocoa.macosx.x86_64_1.1.400.v20160518-1444
  org.eclipse.core.filesystem.macosx_1.3.0.v20140124-1940.jar
  org.eclipse.e4.ui.workbench.renderers.swt.cocoa_0.11.300.v20160330-1418.jar
  org.eclipse.equinox.security.macosx_1.100.200.v20130327-1442.jar
  org.eclipse.jdt.launching.macosx_3.3.0.v20160418-1524.jar
  org.eclipse.jdt.launching.ui.macosx_1.1.0.v20160418-1524.jar
  org.eclipse.swt.cocoa.macosx.x86_64_3.105.1.v20160907-0248.jar
  org.eclipse.ui.cocoa_1.1.100.v20151202-1450.jar
)

function cleanup {
  cd "$DIR"
  rm -rf "$TMP"
}
trap cleanup EXIT

function addPluginLibrary {
  LIB=$1
  SOURCE_DIR=$2
  TARGET_DIR=$3

  if [ -d "$SOURCE_DIR/plugins/$LIB" ]; then
    cp -r "$SOURCE_DIR/plugins/$LIB" "$TARGET_DIR/plugins/"
    sed -i '' "s/osgi.bundles=/osgi.bundles=reference\:file\:$LIB\/@4,/g" "$TARGET_DIR/configuration/config.ini"
  elif [ -f "$SOURCE_DIR/plugins/$LIB" ]; then
    cp "$SOURCE_DIR/plugins/$LIB" "$TARGET_DIR/plugins/"
    sed -i '' "s/osgi.bundles=/osgi.bundles=reference\:file\:$LIB@4,/g" "$TARGET_DIR/configuration/config.ini"
  else
    echo "Unable to add unknown library: $SOURCE_DIR/plugins/$LIB, aborting."
    exit 1
  fi
}

function removePluginLibrary {
  LIB=$1
  TARGET_DIR=$2

  if [ -d "$TARGET_DIR/plugins/$LIB" ]; then
    rm -r "$TARGET_DIR/plugins/$LIB"
    sed -i '' "s/,reference\\\\:file\\\\:$LIB\/@4//g" "$TARGET_DIR/configuration/config.ini"
  elif [ -f "$TARGET_DIR/plugins/$LIB" ]; then
    rm "$TARGET_DIR/plugins/$LIB"
    sed -i '' "s/,reference\\\\:file\\\\:$LIB@4//g" "$TARGET_DIR/configuration/config.ini"
  else
    echo "Cannot remove library $LIB from $TARGET_DIR/plugins, I couldn't locate that library."
    exit 1
  fi
}

# initialize
mkdir -p "$DOWNLOAD_DIR"
mkdir -p "$TMP"

if [ ! -d "$DOWNLOAD_DIR/IntershopStudio" ]; then
  echo -n "Retrieving and unpacking Intershop Studio package ..."
  unzip -q -d "$DOWNLOAD_DIR" "$IS_ARCHIVE"
  echo "done"
fi

# Download / untar Mac OS X Eclipse if not downloaded already
if [ ! -d "$DOWNLOAD_DIR/Eclipse.app" ]; then
  echo "Retrieving base Mac OS X Eclipse package..."
  $CURL --remote-header-name "$MAC_ECLIPSE_URL" | $TAR xz -C "$DOWNLOAD_DIR"
  echo "done"
fi

# We take the original Mac OS X edition as baseline:
echo -n "Copying Mac OS X baseline Eclipse.app to temporary directory..."
cp -r "$DOWNLOAD_DIR/Eclipse.app" "$TMP/Eclipse.app"
echo " done"

# We replace the contents of Eclipse.app/Contents/Eclipse with the Studio files
echo -n "Replacing baseline Eclipse.app contents with IntershopStudio version..."
rm -rf "$TMP/Eclipse.app/Contents/Eclipse"
cp -r "$DOWNLOAD_DIR/IntershopStudio" "$TMP/Eclipse.app/Contents/Eclipse"
echo " done"

# Remove linux specific libraries
echo -n "Removing linux specific libraries..."
for i in "${LIBRARIES_TO_REMOVE[@]}"; do
  removePluginLibrary $i "$TMP/Eclipse.app/Contents/Eclipse"
done
echo "done"

# Add MacOS specific libraries
echo -n "Adding Mac OS X specific libraries..."
for i in "${LIBRARIES_TO_ADD[@]}"; do
  addPluginLibrary $i "$DOWNLOAD_DIR/Eclipse.app/Contents/Eclipse" "$TMP/Eclipse.app/Contents/Eclipse"
done
echo "done"

# ini file(s)
echo -n "Changing Eclipse .ini file..."
# Because the main application is 'eclipse' in the base distribution we need to rename the property file
mv "$TMP/Eclipse.app/Contents/Eclipse/IntershopStudio.ini" "$TMP/Eclipse.app/Contents/Eclipse/eclipse.ini"
sed -i '' '
s/^plugins/..\/Eclipse\/plugins/g
s/launcher\.gtk\.linux/launcher.cocoa.macosx/g
$ a\
-Xdock:icon=../Resources/Eclipse.icns
$ a\
-XstartOnFirstThread
$ a\
-Dorg.eclipse.swt.internal.carbon.smallFonts
' "$TMP/Eclipse.app/Contents/Eclipse/eclipse.ini"
echo "done"

mv "$TMP/Eclipse.app" "$DIR/"
